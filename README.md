My final year project (while in my Associate Degree's the first year).
The original readme (in spanish) follows:

----8<----8<----8<----8<----8<----8<----8<----8<----8<----8<----8<----8<----8<


*****************************************************************************
*                                                                           *
*                    NONAME.WAR v1.0 Beta   Documentacion                   *
*                                                                           *
*                       Juan Ignacio Rodriguez Garcia                       *
*****************************************************************************



0.-   Requisitos del sistema


   Minimos:
          486 DX2/66 con 4 megas de RAM
          1 Mega libre de HD
          Tarjeta de video que pueda hacer 640x480x256 colores
          Raton
          Teclado

   Recomendados:

          Pentium 100 o superior , 8 megas de RAM
          3 Megas libres de HD
          Tarjeta de sonido AWE32 o SB16
          Tarjeta de video que pueda hacer 640x480x256 colores
          Raton
          Teclado



     Tambien es necesario un servidor de DPMI, por lo que es necesario
     ejecutar el programa de una de estas maneras:

          1) Arrancandolo desde una ventana DOS de Win95
          2) Copiando los ficheros del directorio MISC hacia donde
             este war.exe


1.-   �Que es NONAME.WAR?

   Como ser , no es nada.
   Pero pretendia ser un juego multijugador del tipo CTF (Capture the Flag).
   Por motivos tecnicos en la comunicacion por puerto serie , no se ha podido
   entregar con total funcionalidad.
   Si lo que se desea es probar el juego , existen varias teclas habilitadas
   para tal efecto , y son:

   F12:   Pulsando esta tecla , podemos pasar por alto la conexion con el
          otro PC , y asi comenzar una partida donde el enemigo esta quieto.

   F5,F6,F7:   Con estas otras podemos atacar a nuestras unidades , como si
               de un enemigo se tratara.
               Gracias a ello , podemos ver como pierden vida, mueren ,etc.
               Incluso cuando llevan la bandera enemiga.

   Espacio :   Esta tecla ha sido mi gran aliada durante el desarrollo del
               programa.Con ella , ponia en pantalla las variables principa-
               les del juego , para asi poder detectar fallas.




2)  �Como se juega a NONAME.WAR?


      El objetivo principal del juego es capturar la bandera enemiga y no
      morir en el intento.
      Por supuesto , nuestro contrincante tiene el mismo objetivo , asi que
      gana el primero que capture la bandera del otro o el que derrote en
      combate las unidades del otro.
      Cada unidad tiene disponibles 3 tipos de armas:

      JAR-XP:    Dispones de 20 balas de este tipo.Son de potencia baja.
      PARGUEBUM: Este arma con pinta de col arrocera es un buen espanta-
                 enemigos.
      ACANDEMOR: Con este arma , tus enemigos saldr� corriendo del lugar.
                 Si les da tiempo.


      Para seleccionar a una unidad , solo tienes que hacer click! con el
      boton izquierdo del raton cuando estes con el cursor encima de ella.
      Pulsando sobre otro punto , tu unidad __intentara__ llegar hasta alli,
      salvo que el trayecto sea complicado.
      Si deseas parar una unidad seleccionada que se encuentre en movimiento
      solo tienes que pulsar el boton derecho del raton sobre cualquier punto.
      En ese momento , el marco de alrededor de la unidad , se pondra oscuro
      y parar� en cuanto pueda.

      Para cambiar de arma , solo tienes que se�alarla en el panel de la dere-
      cha. Si tu arma no tiene municiones , no podras disparar.
      El indicador de la derecha , tiene el numero de proyectiles que te quedan.

      Para moverte por el escenario tienes dos opciones:
      - Poner el cursor del raton cerca de los bordes, y asi el escenario
        correra en la direccion deseada.
      - Usando las teclas de los cursores.


3)    �Como se captura una bandera en NONAME.WAR?

      Cada equipo tiene una base , representada por un circulo de color , sobre
      el que descansa la bandera.
      Para capturar la bandera enemiga , tienes que situar una de tus unidades
      sobre ella y llevarla hacia tu base (en la esquina opuesta).
      Si tu unidad es derribada mientras transportaba la bandera ,esta se
      depositara en el suelo en el lugar de la unidad derrotada.




4)    Datos tecnicos del juego




        He optado por un sistema de celdas , para ahorrar memoria y para que
        la edicion del nivel fuese mas sencilla.
        Existe un fichero "mapa.txt" , donde con caracteres se especifica
        el objeto que debe existir en cada casilla.
        Esto ha supuesto un problema a la hora de representar un movimiento
        suave de las unidades , ya que no podian ir de casilla en casilla
        alegremente.
        Este proyecto es una mezcla entre C y C++ :
        En C++ se han programado todos los objetos del juego (Escenario,
        soldados,armas,banderas,etc) y en C las interacciones entre los
        mismos.
        He usado un compilador gratuito y Open Source para DOS , que es
        el DJGPP (http://www.delorie.com) que soporta arquitectura de 32 bits
        y compatibilidad parcial con Unix.
        Tambien me he servido de las librerias ALLEGRO 3.11 , que me han
        resuelto :)  todo el tema de graficos y sonido.
        Respecto al sonido , solo decir que la musica del juego esta en
        formato MIDI , asi que es necesario disponer de una tarjeta de
        sonido con sintesis de tabla de ondas para poder escucharla
        con "decencia". Si no se dispone de ella , NONAME.WAR usara una
        sintesis de tabla de ondas por Software (usando "patches.dat"),
        que ofrece una calidad mayor que la musica por OPL.
        La tarjeta de sonido se autodetecta al comenzar , asi como la
        de video.

        Poco queda por comentar.
        Tan solo que "inexplicablemente" , siendo un programa para MSDOS,
        funciona mucho mejor en una ventana de DOS de Windows95/98.


5) El futuro del juego.


        Ya ha comenzado un porte de este juego para Linux , donde se
        esperan solucionar los problemas de comunicaciones.
        Se usar� TCP/IP y soportara hasta 4 jugadores simultaneos.







                                         Sevilla, 16 de Junio de 1999
