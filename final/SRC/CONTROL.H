#ifndef _CONTROL_H_
#define _CONTROL_H_




void init_armas(void)
{
 pArma[0]=new CArma(0,25,0,CABEZA,"JAR",0);
 pArma[1]=new CArma(0,80,0,MANO_D,"PARGUEBUM",1);
 pArma[2]=new CArma(0,125,0,PECHO,"ACANDEMOR",2);
}

//------------------------------------------------------------------
//------------------------------------------------------------------

CArma * get_arma(const char *nombre)
{
   int i=0,enc=0;

   while(i<MAX_ARMAS && !enc)
   {
      if(strcmp(nombre,pArma[i]->get_nombre())==0)
      {
         enc=1;
      }
      else
          i++;
   }

   if(enc)
     return pArma[i];
   else
     return NULL;
}

//------------------------------------------------------------------
//------------------------------------------------------------------


void end_armas(void)
{
 int i;

 for(i=0;i<MAX_ARMAS;i++)
    delete pArma[i];

}

//------------------------------------------------------------------
//------------------------------------------------------------------


void init_texturas(void)
{

}

//------------------------------------------------------------------
//------------------------------------------------------------------

void end_texturas(void)
{

}

//------------------------------------------------------------------
//------------------------------------------------------------------

void init_video(void)
{
    set_palette((PALETTE)data[GAME_PAL].dat);
    pantalla=create_bitmap(640,480);
    escenario=create_bitmap(VISIBLE_X*SQR_X,VISIBLE_Y*SQR_Y);
    minimapa=create_bitmap((MAX_SCN_X-1)*2,MAX_SCN_Y*2);

    clear(pantalla);
    clear(escenario);
    clear_to_color(minimapa,17);
    clear(screen);

    set_mouse_sprite_focus(1,1);
    set_mouse_sprite((BITMAP *)data[MOUSE].dat);

}

//------------------------------------------------------------------
//------------------------------------------------------------------

void end_video(void)
{
   destroy_bitmap(pantalla);
   destroy_bitmap(escenario);
}

//------------------------------------------------------------------
//------------------------------------------------------------------

void frame(void)
{
 static int p1=-1,p2,p3,p4;

 if(p1==-1) //para acelerar y no calcular mas de la cuenta
 {
   p1=OFFSET_X*SQR_X;
   p2=OFFSET_Y*SQR_Y;
   p3=VISIBLE_X*SQR_X;
   p4=VISIBLE_Y*SQR_Y;
 }

 show_mouse(NULL);
 blit(escenario,pantalla,0,0,p1,p2,p3,p4);
 show_mouse(pantalla);
 blit(pantalla,screen,0,0,0,0,640,480);
 frames++;

//para suavizar el raton:
boton_raton_i=mouse_b&1;
boton_raton_d=(mouse_b&2)/2;
}

//------------------------------------------------------------------
//------------------------------------------------------------------


void representar_unidad(CUnidad *unidad,CEscenario *scn)
{
   int x,y;
   int ix,iy,fx,fy;
   int offset_x,offset_y;

   scn->get_rango(&ix,&iy,&fx,&fy);
   unidad->get_xy(&x,&y);

   if( x>=ix && x<=fx && y>=iy && y<=fy )
   {
      switch(unidad->get_orientacion())
        {
         case ARRIBA:
         case ABAJO:
              offset_x=0;
              offset_y=unidad->offset_mov;
              break;
         case IZQUIERDA:
         case DERECHA:
              offset_x=unidad->offset_mov;
              offset_y=0;
              break;
         default:
              offset_x=0;
              offset_y=0;
              break;
        }

      if(unidad->get_HP()>0)
      {
         rotate_sprite(escenario,(BITMAP *)data[SOLD1].dat,(x-ix)*SQR_X+offset_x,(y-iy)*SQR_Y+offset_y,itofix(unidad->get_ult_orientacion()));
         if(unidad->fase_impacto!=-1)
         {
            switch(unidad->fase_impacto)
            {
                case 0:
                case 7:
                    draw_sprite(escenario,(BITMAP *)data[EXP1].dat,(x-ix)*SQR_X+offset_x,(y-iy)*SQR_Y+offset_y);
                    break;
                case 1:
                case 6:
                    draw_sprite(escenario,(BITMAP *)data[EXP2].dat,(x-ix)*SQR_X+offset_x,(y-iy)*SQR_Y+offset_y);
                    break;
                case 2:
                case 5:
                    draw_sprite(escenario,(BITMAP *)data[EXP3].dat,(x-ix)*SQR_X+offset_x,(y-iy)*SQR_Y+offset_y);
                    break;
                case 3:
                case 4:
                    draw_sprite(escenario,(BITMAP *)data[EXP4].dat,(x-ix)*SQR_X+offset_x,(y-iy)*SQR_Y+offset_y);
                    break;

            }
         }

         if(unidad->isselected())
         {
            if(unidad->get_wannastop() && unidad->moviendo())
                rect(escenario,(x-ix)*SQR_X+offset_x,(y-iy)*SQR_Y+offset_y,(x-ix)*SQR_X+SQR_X-1+offset_x,(y-iy)*SQR_Y+SQR_Y-1+offset_y,190);
            else
                rect(escenario,(x-ix)*SQR_X+offset_x,(y-iy)*SQR_Y+offset_y,(x-ix)*SQR_X+SQR_X-1+offset_x,(y-iy)*SQR_Y+SQR_Y-1+offset_y,80);
         }
         rectfill(escenario,(x-ix)*SQR_X+offset_x+4,(y-iy)*SQR_Y+offset_y,(x-ix)*SQR_X+offset_x+4+((unidad->get_HP()*12)/255),(y-iy)*SQR_Y+offset_y+2,90);
      }
      else
        rotate_sprite(escenario,(BITMAP *)data[SOLD1D].dat,(x-ix)*SQR_X+offset_x,(y-iy)*SQR_Y+offset_y,itofix(unidad->get_ult_orientacion()));
   }
}

//------------------------------------------------------------------
//------------------------------------------------------------------


void representar_enemigo(CEnemigo *unidad,CEscenario *scn)
{
   int x,y;
   int ix,iy,fx,fy;
   int offset_x,offset_y;

   scn->get_rango(&ix,&iy,&fx,&fy);
   unidad->get_xy(&x,&y);

   if(scn->es_visible(x,y)==0 || scn->es_visible(x,y)==2)
      return;

   if( x>=ix && x<=fx && y>=iy && y<=fy )
   {
      switch(unidad->get_orientacion())
        {
         case ARRIBA:
         case ABAJO:
              offset_x=0;
              offset_y=unidad->offset_mov;
              break;
         case IZQUIERDA:
         case DERECHA:
              offset_x=unidad->offset_mov;
              offset_y=0;
              break;
         default:
              offset_x=0;
              offset_y=0;
              break;
        }


      if(unidad->get_HP()>0)
      {
         rotate_sprite(escenario,(BITMAP *)data[ENEM1].dat,(x-ix)*SQR_X+offset_x,(y-iy)*SQR_Y+offset_y,itofix(unidad->get_orientacion()));

         if(unidad->fase_impacto!=-1)
         {
            switch(unidad->fase_impacto)
            {
                case 0:
                case 7:
                    draw_sprite(escenario,(BITMAP *)data[EXP1].dat,(x-ix)*SQR_X+offset_x,(y-iy)*SQR_Y+offset_y);
                    break;
                case 1:
                case 6:
                    draw_sprite(escenario,(BITMAP *)data[EXP2].dat,(x-ix)*SQR_X+offset_x,(y-iy)*SQR_Y+offset_y);
                    break;
                case 2:
                case 5:
                    draw_sprite(escenario,(BITMAP *)data[EXP3].dat,(x-ix)*SQR_X+offset_x,(y-iy)*SQR_Y+offset_y);
                    break;
                case 3:
                case 4:
                    draw_sprite(escenario,(BITMAP *)data[EXP4].dat,(x-ix)*SQR_X+offset_x,(y-iy)*SQR_Y+offset_y);
                    break;

            }

         }
         rect(escenario,(x-ix)*SQR_X+offset_x,(y-iy)*SQR_Y+offset_y,(x-ix)*SQR_X+SQR_X-1+offset_x,(y-iy)*SQR_Y+SQR_Y-1+offset_y,30);
         rectfill(escenario,(x-ix)*SQR_X+offset_x+4,(y-iy)*SQR_Y+offset_y,(x-ix)*SQR_X+offset_x+4+((unidad->get_HP()*12)/255),(y-iy)*SQR_Y+offset_y+2,90);
      }
      else
         rotate_sprite(escenario,(BITMAP *)data[ENEM1D].dat,(x-ix)*SQR_X+offset_x,(y-iy)*SQR_Y+offset_y,itofix(unidad->get_orientacion()));

   }

}

//------------------------------------------------------------------
//------------------------------------------------------------------


void mover(CUnidad *unidad,int x,int y)
{
   unidad->mover_unidad(x,y);
}

//------------------------------------------------------------------
//------------------------------------------------------------------

void sacar_info(CUnidad *unidad)
{

    if(unidad->get_HP()>0)
         draw_sprite(pantalla,(BITMAP *)data[SOLD1].dat,560,50);
    else
         draw_sprite(pantalla,(BITMAP *)data[SOLD1D].dat,560,50);

	textprintf(pantalla,font,525,90,60,"Vida: %03d/255",unidad->get_HP());

    rect(pantalla,506,140,639,290,80);

    draw_sprite(pantalla,(BITMAP *)data[ARMA1].dat,515,150);
    draw_sprite(pantalla,(BITMAP *)data[ARMA2].dat,515,200);
    draw_sprite(pantalla,(BITMAP *)data[ARMA3].dat,515,250);

	textprintf(pantalla,font,600,160,60,"[%02d]",unidad->pEquipo->municion_item(CABEZA));
	textprintf(pantalla,font,600,210,60,"[%02d]",unidad->pEquipo->municion_item(MANO_D));
	textprintf(pantalla,font,600,260,60,"[%02d]",unidad->pEquipo->municion_item(PECHO));

    switch(unidad->pEquipo->get_item_seleccionado())
    {
        case CABEZA:
               rect(pantalla,515,150,595,180,90);
               break;

        case MANO_D:
               rect(pantalla,515,200,595,230,90);
               break;

        case PECHO:
               rect(pantalla,515,250,595,280,90);
               break;

        default:
               break;
    }

    if(Bandera[BANDERA_ENEMIGA].unidad_que_la_tiene==unidad->unidad_id)
    {
     textprintf(pantalla,font,520,320,80,"Tienes la");
     textprintf(pantalla,font,520,330,80,"!BANDERA!");
    }

}

//------------------------------------------------------------------
//------------------------------------------------------------------

void cancelar_selecciones(CUnidad *ejercito[N_SOLDADOS])
{
 int i;

 for(i=0;i<N_SOLDADOS;i++)
    ejercito[i]->select(0);

 alguno_seleccionado=0;
 unidad_seleccionada=-1;
}


//----------------------------------------------------------------------//
//----------------------------------------------------------------------//



void onclick(int x,int y,CUnidad *e[N_SOLDADOS],CEscenario *scn)
{
 int realx,realy;
 int ux,uy,ix,fx,iy,fy,ex,ey;
 int i;
 static int last_selected=-1;
 int flag=-1; //Comprobar si se ha pinchado en otro lugar
 int enemigo_focus;
 static int canal_digital=-1;
 int offset_x,offset_y;
 char net_data;
// char buffer[40];



 if(x<(VISIBLE_X+1)*SQR_X)
 {


 scn->get_rango(&ix,&iy,&fx,&fy);

 realx=x+ix*SQR_X-OFFSET_X*SQR_X;
 realy=y+iy*SQR_Y-OFFSET_Y*SQR_Y;

// sprintf(buffer,"%d,%d",realx,realy);
// alert("Raton",buffer,"","Ok","Cancelar",0,0);

 if(boton_raton_d && boton_raton_i)
   {
    cancelar_selecciones(e);
    last_selected=-1;
    return;
   }
 else
 if(boton_raton_i)
 {

    for(i=0;i<N_SOLDADOS;i++)
    {
      if(e[i]->get_HP()>0)
      {
        e[i]->get_xy(&ux,&uy);

      switch(e[i]->get_orientacion())
        {
         case ARRIBA:
         case ABAJO:
              offset_x=0;
              offset_y=e[i]->offset_mov;
              break;
         case IZQUIERDA:
         case DERECHA:
              offset_x=e[i]->offset_mov;
              offset_y=0;
              break;
         default:
              offset_x=0;
              offset_y=0;
              break;
        }

        if(realx>ux*SQR_X+offset_x && realx<ux*SQR_X+offset_x+SQR_X && realy<uy*SQR_Y+offset_y+SQR_Y && realy>uy*SQR_Y+offset_y)
        {
           if(canal_digital==-1)
           {
              canal_digital=allocate_voice((SAMPLE *)data[ROBOT].dat);
              voice_set_volume(canal_digital,300);
              voice_start(canal_digital);
           }
           else
           if(voice_get_position(canal_digital)==-1)
           {
              deallocate_voice(canal_digital);
              canal_digital=-1;
           }

           e[i]->select(1);
           flag=1;
           last_selected=i;
           alguno_seleccionado=1;
		   unidad_seleccionada=i;
        }
        else
        if(alguno_seleccionado==1)
        {
           e[i]->select(0);
        }
      }
    }

       //Miremos si hemos apuntado a algun enemigo
       enemigo_focus=-1;
       i=0;
       while(i<N_SOLDADOS && enemigo_focus==-1)
       {
           enemigos[i]->get_xy(&ex,&ey);
           if(realx>ex*SQR_X && realx<ex*SQR_X+SQR_X && realy<ey*SQR_Y+SQR_Y && realy>ey*SQR_Y)
              enemigo_focus=i;
           else
               i++;
       }


    if(last_selected!=-1 && flag==-1 && enemigo_focus==-1)
    {
       e[last_selected]->select(1);
       mover(e[last_selected],realx/SQR_X,realy/SQR_Y);
    }
    else if(last_selected!=-1 && flag==-1 && enemigo_focus!=-1)
    {
        atacar_enemigo(e[last_selected],enemigos[enemigo_focus],50);

        net_data=0xC0;                          //codigo de ataque
        net_data=net_data | (enemigo_focus*16); //unidad atacada
        net_data=net_data | (last_selected*4);  //unidad atacante
        net_data=net_data | 3;                  //intensidad

        enviar_serie(net_data);
    }
  }
 else
 if(boton_raton_d)
 {
 for(i=0;i<N_SOLDADOS;i++)
    {
        if(e[i]->isselected()) e[i]->set_stop(1);
    }
  }


 }
 else
 {
   //Son cosas del panel
   if(alguno_seleccionado)
   {

       if(x>515 && x<595)
       {
          if(y>150 && y<180)
          {
             //seleccionamos arma 1
			 e[last_selected]->pEquipo->set_seleccionado(CABEZA);
          }
          else
          if(y>200 && y<230)
          {
             //seleccionamos arma 2
			 e[last_selected]->pEquipo->set_seleccionado(MANO_D);
          }
          else
          if(y>250 && y<280)
          {
             //seleccionamos arma 3
			 e[last_selected]->pEquipo->set_seleccionado(PECHO);
          }
       }
   }


 }


}

//------------------------------------------------------------------
//------------------------------------------------------------------

void scroll(CEscenario *scn)
{
      if((mouse_x/SQR_X == VISIBLE_X || mouse_x/SQR_X == VISIBLE_X+1 && mouse_y/SQR_Y <=VISIBLE_Y+1) || key[KEY_RIGHT])
        scn->scroll(1,0);
      if((mouse_y/SQR_Y == VISIBLE_Y || mouse_y/SQR_Y == VISIBLE_Y+1 && mouse_x/SQR_X <=VISIBLE_X+1) || key[KEY_DOWN])
        scn->scroll(0,1);
      if((mouse_x < OFFSET_X*SQR_X*2 && mouse_y/SQR_Y <=VISIBLE_Y+1) || key[KEY_LEFT])
        scn->scroll(-1,0);
      if((mouse_y < OFFSET_Y*SQR_Y*2 && mouse_x/SQR_X <=VISIBLE_X+1) || key[KEY_UP])
        scn->scroll(0,-1);
}

//------------------------------------------------------------------
//------------------------------------------------------------------


void dibujar_interfaz(void)
{
 int i,j;

 for(i=0;i<=VISIBLE_X+1;i++)
    for(j=0;j<=VISIBLE_Y+1;j++)
       {
        if(!(i>0 && j>0 && i<VISIBLE_X && j<VISIBLE_Y))
        draw_sprite(pantalla,(BITMAP *)data[MARCO].dat,i*SQR_X,j*SQR_Y);
       }

 blit((BITMAP *)data[PANEL].dat,pantalla,0,0,504,0,136,480);

}

//------------------------------------------------------------------
//------------------------------------------------------------------

//Funcion para hacer cuadrados de 4 pixels rapidamente

inline void mrect(BITMAP *_bmp,int x1,int y1,int x2,int y2,int color)
{
   _putpixel(_bmp,x1,y1,color);
   _putpixel(_bmp,x2,y1,color);
   _putpixel(_bmp,x1,y2,color);
   _putpixel(_bmp,x2,y2,color);
}

void dibujar_minimapa(CUnidad *ejercito[N_SOLDADOS],CEscenario *scn,CEnemigo *enemigos[N_SOLDADOS])
{
 int i;
 int x,y;
 int ix,iy,fx,fy;
 int mpx,mpy;

 for(x=0;x<MAX_SCN_X-1;x++)
   for(y=0;y<MAX_SCN_Y;y++)
   {
     mpx=x*2;
     mpy=y*2;
     if(scn->es_visible(x,y)==0)
         mrect(minimapa,mpx,mpy,mpx+1,mpy+1,231);
     else
         mrect(minimapa,mpx,mpy,mpx+1,mpy+1,105);
   }


 for(i=0;i<N_SOLDADOS;i++)
 {
    ejercito[i]->get_xy(&x,&y);
    mpx=x*2; mpy=y*2;
    mrect(minimapa,mpx,mpy,mpx+1,mpy+1,28);
    enemigos[i]->get_xy(&x,&y);
    mpx=x*2; mpy=y*2;
    if(scn->es_visible(x,y))
        mrect(minimapa,mpx,mpy,mpx+1,mpy+1,32);
 }

 scn->get_rango(&ix,&iy,&fx,&fy);
 rect(minimapa,ix*2,iy*2,(fx*2)-2,fy*2-2,17);

 draw_sprite(pantalla,minimapa,520,390);
}

//------------------------------------------------------------------
//------------------------------------------------------------------


void dibujar_ejercito(CUnidad *ejercito[N_SOLDADOS],CEscenario *scn)
{
 int i;

  for(i=0;i<N_SOLDADOS;i++)
  {
   representar_unidad(ejercito[i],scn);
  }

}

//------------------------------------------------------------------
//------------------------------------------------------------------


void dibujar_todo(CUnidad *ejercito[N_SOLDADOS],CEscenario *scn)
{

   scn->draw();

   dibujar_ejercito(ejercito,scn);

   representar_enemigo(enemigos[0],scn);
   representar_enemigo(enemigos[1],scn);
   representar_enemigo(enemigos[2],scn);

   Bandera[BANDERA_AMIGA].draw(escenario);
   Bandera[BANDERA_ENEMIGA].draw(escenario);


   dibujar_interfaz();

   if(unidad_seleccionada!=-1)
       sacar_info(ejercito[unidad_seleccionada]);

   dibujar_minimapa(ejercito,scn,enemigos);

   frame();
}

//------------------------------------------------------------------
//------------------------------------------------------------------


void init_enemigos(void)
{
   enemigos[0]=new CEnemigo(10,15);
   enemigos[1]=new CEnemigo(13,15);
   enemigos[2]=new CEnemigo(12,17);
}
//------------------------------------------------------------------
//------------------------------------------------------------------


void end_enemigos(void)
{
   delete(enemigos[0]);
   delete(enemigos[1]);
   delete(enemigos[2]);
}

//------------------------------------------------------------------
//------------------------------------------------------------------


void update_cursor(int x,int y,CEnemigo *e[N_SOLDADOS],CEscenario *scn)
{
 int flag=0;
 int i=0;
 int ix,iy,fx,fy,realx,realy;
 int ex,ey;

 if(alguno_seleccionado)
 {

 scn->get_rango(&ix,&iy,&fx,&fy);

 realx=(x/SQR_X)+ix-OFFSET_X;
 realy=(y/SQR_X)+iy-OFFSET_Y;

 while(!flag && i<N_SOLDADOS)
   {
     e[i]->get_xy(&ex,&ey);
     if(ex==realx && ey==realy)
     {
      set_mouse_sprite((BITMAP *)data[CURSAT].dat);
      set_mouse_sprite_focus(10,10);
      flag=1;
     }
     else
       i++;
   }

   if(!flag)
   {
       set_mouse_sprite_focus(1,1);
       set_mouse_sprite((BITMAP *)data[MOUSE].dat);
   }
  }
}



#endif
