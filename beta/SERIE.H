#ifndef _SERIE_H_
#define _SERIE_H_


//----------------------------------------------------------------------//
//----------------------------------------------------------------------//
//----------------------------------------------------------------------//
//----------------------------------------------------------------------//
//----------------------------------------------------------------------//
//
//   FUNCIONES GESTORAS COMUNICACION PUERTO SERIE
//
//----------------------------------------------------------------------//
//----------------------------------------------------------------------//
// Protocolo de datos:
//
//
//-----------------------------------------------------------------------


void flog(unsigned char d)
{
// fprintf(f,"%c\n",d);
}


char leer_serie(unsigned char *dato)
{
   int status,ret=0,dato_serie;

   status=bioscom(3,0,PUERTO_SERIE);

   if(status&0x100) //dato preparado
   {
       dato_serie=bioscom(2,0,PUERTO_SERIE);
       *dato=dato_serie & 0xFF;
       ret=1;
   }
   return ret;
}

void enviar_serie(unsigned char dato)
{
    if(conectados)
        bioscom(1,dato,PUERTO_SERIE);
}

void tratar_dato(unsigned char dato,int unidad,int orden)
{
    switch(orden)
    {
        case 0:
             {
                //Leemos la X y la unidad mia a la que ataca su unidad
                enemigos[unidad]->set_x(dato&0x3F);
             }
             break;
        case 1:
             {
                //Leemos la Y y datos varios
                enemigos[unidad]->set_y(dato&0x3F);
             }
             break;
        default:
             break;
    }
}



void update_enemigos(unsigned char dato)
{
     static int unidad=0,d=0;

     tratar_dato(dato,unidad,d);
     if(d==1)
     {
        d=0;
        unidad++;
        if(unidad==N_SOLDADOS)
          unidad=0;
     }
     else
       d++;

}

void update_soldados(void)
{
     static int unidad=0,d=0;
     unsigned char dato=0x00;


     switch(d)
     {
         case 0:
              dato=0x00;  //00 000000
              dato=dato | (unsigned char)ejercito[unidad]->get_x();
              flog('0');
              break;
         case 1:
              dato=0x40;  //01 000000
              dato=dato | (unsigned char)ejercito[unidad]->get_y();
              flog('1');
              break;
     }

     enviar_serie(dato);

     flog(dato);

     if(d==1)
     {
        d=0;
        unidad++;
        if(unidad==N_SOLDADOS)
          unidad=0;
     }
     else
       d++;

}

void update_ataques(unsigned char dato)
{
    int unidad_atacada,unidad_atacante,intensidad;

    //Destripamos el mensaje ...
    unidad_atacada= (dato&0x30)-15;
    unidad_atacante=(dato&0x0C)-3;
    intensidad=     (dato&0x03);

    //....y tomamos la accion oportuna
    atacar_mio(enemigos[unidad_atacante],ejercito[unidad_atacada],intensidad);
}


void terminate_game(int modo)
{
    switch(modo)
    {
        case GANAMOS:
                     alert("!Has GANADO!","","","Ok",NULL,1,2);
                     break;
        case PERDIMOS:
                     alert("!Has PERDIDO!","","","Ok",NULL,1,2);
                     break;
    }
}


void sr_tomar_decision(unsigned char dato)
{
   char subcomando;

   subcomando=dato&0x30;

   switch(subcomando)
   {
    case SR_SC_FIN_COMM:
                        alert("Comunicacion","TERMINADA","","Ok",NULL,1,2);
                        break;
    case SR_SC_CAPTURADOS:
                        alert("Tu Bandera","ha sido","!CAPTURADA!","Ok",NULL,1,2);
                        terminate_game(PERDIMOS);
                        break;
    case SR_SC_ENEM_DESTR:
                        alert("Has destruido","al","!ENEMIGO!","Ok",NULL,1,2);
                        terminate_game(GANAMOS);
                        break;
    default:
                        break;
   }

}

void leer_datos_comunicacion(void)
{
    unsigned char dato,tipo_dato;
    int ok;

    ok=leer_serie(&dato);
    if(ok)
    {
         tipo_dato=dato&0xC0;  //Los dos bits mas altos

         switch((unsigned char)tipo_dato)
         {
             case SR_TIPO_POSICION_X:
             case SR_TIPO_POSICION_Y:
                   update_enemigos(dato);
                   break;
             case SR_TIPO_ATAQUE:
                   update_ataques(dato);
                   break;
             case SR_TIPO_COMANDO:
                   sr_tomar_decision(dato);
                   break;
             default:
                   //Fallo en la comunicacion...
                   break;
         }
    }
}

void update_net_data(void)
{
     leer_datos_comunicacion();
     update_soldados();
}


#endif
