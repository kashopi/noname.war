#ifndef _WLIB_H_
#define _WLIB_H_


#define MAX_TXT_SUBMENU 20
#define MAX_SUBMENU 6

class CSubMenu
{
 private:
         int accion;
         char texto[MAX_TXT_SUBMENU];
 public:
        CSubMenu();
        ~CSubMenu();

        void set_accion(int _p){ accion=_p; };
        void set_texto(char *txt){ strncpy(texto,txt,MAX_TXT_SUBMENU); };
        int get_accion(void){ return accion; };
        char * get_texto(void){ return texto; };
};



class CBarraMenu:public CSubMenu
{
 private:
         int x,y;
         BITMAP *barra,*fondo_menu;
         CSubMenu *pSubmenu[MAX_SUBMENU];
         int en_pantalla;
 public:
        //Constructor y destructor
        CBarraMenu(int _x=mouse_x,int _y=mouse_y);
        ~CBarraMenu();

        //Metodos de la clase
        void set_xy(int _x,int _y){ x=_x; y=_y; };
        void get_xy(int *_x,int *_y){ *_x=x; *_y=y; };
        void add_menu(char *texto,int accion);
        void draw(BITMAP *_bmp);
        int visible(void){ return en_pantalla; };
        void hide(void){ en_pantalla=0; };
};

#endif
